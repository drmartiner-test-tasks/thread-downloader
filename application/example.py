from .downloader import Downloader

if __name__ == "__main__":
    urls_to_save = [
        "https://docs.python.org",
        "https://www.google.com",
        "https://www.djangoproject.com",
        "https://github.com",
    ]

    downloader = Downloader()
    downloader.start(urls_to_save)

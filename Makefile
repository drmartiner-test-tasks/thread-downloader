.PHONY: isort
isort:
	@echo -n "Run isort"
	isort -rc application


.black: $(shell find application -type d)
	black -l 120 application tests
	@if ! isort -c -rc application; then \
            echo "Import sort errors, run 'make isort' to fix them!!!"; \
            isort --diff -rc application; \
            false; \
	fi

.PHONY: black
black: .black
	@echo -n "Run black & isort"

.PHONY: test
test:
	@echo -n "Run tests"
	$(eval export PYTHONPATH=.)
	py.test -svvv -rs

.PHONY: run
run:
	python -m application.example

.PHONY: clean
clean:
	@echo -n "Clear temp files"
	@rm -rf `find . -name __pycache__`
	@rm -rf `find . -name .tox`
	@rm -rf `find . -name *.egg-info`
	@rm -rf `find . -name .pytest_cache`
	@rm -rf `find . -name .cache`
	@rm -rf `find . -name htmlcov`
	@rm -rf `find . -name .coverage`
	@rm -rf `find . -name dist`
	@rm -rf `find . -name build`
	@rm -rf `find . -name pages`

import os
from functools import partial
from multiprocessing import Manager, Pool, cpu_count
from multiprocessing.synchronize import Lock
from pathlib import Path
from typing import Dict, List, Union
from urllib.parse import urlparse

import requests

from . import config


class Downloader:
    def __init__(self) -> None:
        self.create_download_folder()

    @classmethod
    def create_download_folder(cls) -> None:
        if os.path.exists(config.DOWNLOAD_FOLDER):
            assert os.path.isdir(config.DOWNLOAD_FOLDER)
        else:
            os.mkdir(config.DOWNLOAD_FOLDER)

    def start(self, urls) -> None:
        processes_count = config.PROCESSES_COUNT or cpu_count()
        with Pool(processes=processes_count) as pool:
            with Manager() as manager:
                lock = manager.Lock()
                downloaded_pages = manager.list()

                func = partial(self._load_page, lock, downloaded_pages)
                pool.map(func, urls)

                for i in range(10):
                    pool.apply(self._save_page, (lock, downloaded_pages))

                pool.close()
                pool.join()

    def _load_page(self, lock: Lock, pages: List[Dict[str, str]], url) -> None:
        html = self._get_html(url)
        self._put_loaded_page(lock, pages, url, html)

    def _get_html(self, url) -> str:
        response = requests.get(url)
        response.raise_for_status()

        return response.text

    def _put_loaded_page(self, lock: Lock, pages: List[Dict[str, str]], url: str, html: str) -> None:
        lock.acquire()
        try:
            pages.append({"url": url, "html": html})
        except Exception as e:
            raise e
        finally:
            lock.release()

    def _save_page(self, lock: Lock, pages: List[Dict[str, str]]) -> None:
        page = self._pop_page_for_save(lock, pages)
        if not page:
            return

        parser = urlparse(page["url"])
        file_path = Path(config.DOWNLOAD_FOLDER) / f"{parser.hostname}.html"
        with open(file_path, "w") as f:
            f.write(page["html"])

    def _pop_page_for_save(self, lock, pages) -> Union[str, None]:
        if not pages:
            return

        lock.acquire()
        page = None
        try:
            page = pages.pop()
        except IndexError as e:
            ...
        except Exception as e:
            raise e
        finally:
            lock.release()

        return page

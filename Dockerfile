FROM python:3.7.4

ENV APP_DIR "/app"
ENV PYTHONDONTWRITEBYTECODE 1

RUN mkdir -p ${APP_DIR}
WORKDIR ${APP_DIR}
ADD . ${APP_DIR}

RUN pip install poetry &&\
    poetry config settings.virtualenvs.create false &&\
    poetry install --no-dev

ENTRYPOINT ["/app/docker-entrypoint.sh"]
CMD ["run"]

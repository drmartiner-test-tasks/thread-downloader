# Brief
Develop a multi-thread Python application to fetch, analyse and store data.
The application should have a threadpool (or a set of threadpools) of two different kinds of components:
- The first component should be in charge of fetching the data from a "source" entity, and store it in a message queue
- The second component should be in charge of working on the same queue, by performing some operations on each item through a "processing" entity, and finally sending it to a "destination" entity
The development of the "source", "processing" and "destination" entities is not in the scope of this exercise, so it's up to you to abstract or mock them accordingly.

Tips:
- Ensure access to shared resources is thread-safe
- If you lack of fantasy, you can just use some input JSON objects and "process" them by altering their properties
- Assume the code would eventually need to be extended to support additional sources, destinations or processing entities, so abstract common data structures and/or routines when possible
- Unit/integration tests are more than welcome
- Keep it simple... don't overshoot

Deadline: 2 days 

# Environments
```ini
PROCESSES_COUNT=
WORKERS_LOADER=10
WORKERS_SAVER=5

DOWNLOAD_FOLDER=pages
```

# Develop
```bash
pyenv virtualenv 3.7.4 thread_downloader 
pyenv activate thread_downloader

pip install poetry black

poetry install

make test
make run

make isort black
```

# Deploy
## Prod
```bash
docker run -ti -v $(pwd)/pages:/app/pages registry.gitlab.com/drmartiner-test-tasks/thread-downloader:1.0.0
```

## Local
```bash
docker build .
docker run -ti -v $(pwd)/pages:/app/pages image-id
```

#!/usr/bin/env bash

args=("$@")

case "${1}" in
    "bash")
        shift
        exec bash -c "${args[@]:1}"
        ;;
    "run")
        make run
        ;;
    "test")
        make test
        ;;
    *)
	    exec echo "Use 'run', 'test' or 'bash'"
        ;;
esac
